// main.cpp

#include<Windows.h>
#include<iostream>
#include<string>
#include<cstdio>
#include<ctime>
using namespace std;

#define COND(val, conditional) if(val&conditional) cout << #conditional << endl
#define SELECT(val, description) case val: cout << description << endl; break

typedef unsigned int uint;

int main() {
	string filename;

	// ファイル指定
	cerr << "Read Portable Executable Format : ";
	cin >> filename;
	cout << endl;

	// ファイルを開く
	HANDLE hFile = CreateFile(filename.data(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if(hFile == nullptr) {
		cout << "No File Exists...." << endl;
		return 1;
	}
	cout << "FilePath - " << filename << endl;


	// ファイルマッピングオブジェクト生成
	HANDLE hFileMap = CreateFileMapping(hFile, nullptr, PAGE_READONLY, 0, 0, nullptr);
	// サイズ
	DWORD dwSize = GetFileSize(hFile, 0);
	// ファイルのビューを取得
	LPBYTE lpFileBase = (LPBYTE)MapViewOfFile(hFileMap, FILE_MAP_READ, 0, 0, dwSize);

	// ===== IMAGE_DOS_HEADER =====
	cout << ">>>>> IMAGE_DOS_HEADER" << endl;

	// IMAGE_DOS_HEADERのポインタ
	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)lpFileBase;
	// MZ Header確認(0x5A4D)
	if(pDosHeader->e_magic != IMAGE_DOS_SIGNATURE) {
		cout << "No PE File Format (DOS_SIGNATURE)" << endl;
		return 0;
	}

	cout << "e_magic is IMAGE_DOS_SIGNATURE(0x5A4D)." << endl;

	cout << endl;
	
	// ===== IMAGE_NT_HEADER =====
	cout << ">>>>> IMAGE_NT_HEADER" << endl;

	// IMAGE_NT_HEADERSのポインタ
	PIMAGE_NT_HEADERS pNtHeader = (PIMAGE_NT_HEADERS)((DWORD)pDosHeader + pDosHeader->e_lfanew);
	// Signature Check (PE00)
	if(pNtHeader->Signature != IMAGE_NT_SIGNATURE) {
		cout << "No PE File Format (NT_SIGNATURE)" << endl;
		return 0;
	}

	cout << "Signature is IMAGE_NT_SIGNATURE(0xPE00)." << endl;

	cout << endl;

	// ===== IMAGE_FILE_HEADER =====
	cout << ">>>>> IMAGE_FILE_HEADER" << endl;

	// IMAGE_FILE_HEADERのポインタ
	PIMAGE_FILE_HEADER pFileHeader = &pNtHeader->FileHeader;
	// データ
	cout << "Machine : ";
	switch(pFileHeader->Machine) {
		SELECT(IMAGE_FILE_MACHINE_I386, "x86");
		SELECT(IMAGE_FILE_MACHINE_IA64, "Intel Itanium");
		SELECT(IMAGE_FILE_MACHINE_AMD64, "x64");
	}
	cout << "Number of Sections : " << pFileHeader->NumberOfSections << endl;
	char szTime[26];
	ctime_s(szTime, 26, (const time_t*)&pFileHeader->TimeDateStamp);
	cout << "Time Date Stamp : " << szTime;// << endl;
	cout << "Size of Optional Header : " << pFileHeader->SizeOfOptionalHeader << endl;
	WORD characteristics = pFileHeader->Characteristics;
	cout << "Characteristics : " << endl;
	COND(characteristics, IMAGE_FILE_RELOCS_STRIPPED);         // ベース再配置情報が含まれていない
	COND(characteristics, IMAGE_FILE_EXECUTABLE_IMAGE);        // イメージのみ
	COND(characteristics, IMAGE_FILE_LINE_NUMS_STRIPPED);      // COFF行番号が含まれていない
	COND(characteristics, IMAGE_FILE_LOCAL_SYMS_STRIPPED);     // ローカルシンボルのためのCOFFシンボルが含まれていない
	COND(characteristics, IMAGE_FILE_AGGRESIVE_WS_TRIM);       // Aggressively trim the working set.
	COND(characteristics, IMAGE_FILE_LARGE_ADDRESS_AWARE);     // 2GB以上のアドレスが扱える
	COND(characteristics, IMAGE_FILE_BYTES_REVERSED_LO);       // リトルインディアン
	COND(characteristics, IMAGE_FILE_32BIT_MACHINE);           // 32bitアーキテクチャマシン
	COND(characteristics, IMAGE_FILE_DEBUG_STRIPPED);          // デバッグ情報が含まれていない
	COND(characteristics, IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP); // リムーバブルメディア上にあればスワップファイルからコピーして実行
	COND(characteristics, IMAGE_FILE_NET_RUN_FROM_SWAP);       // ネットワーク上にあればスワップファイルからコピーして実行
	COND(characteristics, IMAGE_FILE_SYSTEM);                  // システムファイル
	COND(characteristics, IMAGE_FILE_DLL);                     // DLLファイル
	COND(characteristics, IMAGE_FILE_UP_SYSTEM_ONLY);          // ユニプロセッサのみで実行すべきファイル
	COND(characteristics, IMAGE_FILE_BYTES_REVERSED_HI);       // ビッグインディアン

	cout << endl;

	// ===== IMAGE_OPTIONAL_HEADER =====
	cout << ">>>>> IMAGE_OPTIONAL_HEADER" << endl;

	// IMAGE_OPTIONAL_HEADERのポインタ
	PIMAGE_OPTIONAL_HEADER pOptionalHeader = (PIMAGE_OPTIONAL_HEADER)&pNtHeader->OptionalHeader;
	// データ
	cout << "Magic : ";
	switch(pOptionalHeader->Magic) {
		SELECT(IMAGE_NT_OPTIONAL_HDR32_MAGIC, "32bit");
		SELECT(IMAGE_NT_OPTIONAL_HDR64_MAGIC, "64bit");
		SELECT(IMAGE_ROM_OPTIONAL_HDR_MAGIC, "ROM Image");
	}
	printf("Size of Code : 0x%X\n", pOptionalHeader->SizeOfCode);
	printf("Base of Code : 0x%X\n", pOptionalHeader->BaseOfCode);
	printf("Linker Version : %d.%.2d\n", pOptionalHeader->MajorLinkerVersion, pOptionalHeader->MinorLinkerVersion);
	printf("ImageBase : 0x%X\n", pOptionalHeader->ImageBase);
	printf("Address of EntryPoint : 0x%X\n", pOptionalHeader->AddressOfEntryPoint);
	printf("Section Alignment : 0x%X\n", pOptionalHeader->SectionAlignment);
	printf("File Alignment : 0x%X\n", pOptionalHeader->FileAlignment);
	printf("Size of Image : 0x%X\n", pOptionalHeader->SizeOfImage);
	printf("Size of Header : 0x%X\n", pOptionalHeader->SizeOfHeaders);
	cout << "Subsystem : ";
	switch(pOptionalHeader->Subsystem) {
		SELECT(IMAGE_SUBSYSTEM_UNKNOWN, "Unknown");
		SELECT(IMAGE_SUBSYSTEM_NATIVE, "No subsystem required (*.sys, etc...)");
		SELECT(IMAGE_SUBSYSTEM_WINDOWS_GUI, "Windows GUI");
		SELECT(IMAGE_SUBSYSTEM_WINDOWS_CUI, "Windows CUI");
		SELECT(IMAGE_SUBSYSTEM_OS2_CUI, "OS/2 CUI");
		SELECT(IMAGE_SUBSYSTEM_POSIX_CUI, "Posix CUI");
		SELECT(IMAGE_SUBSYSTEM_WINDOWS_CE_GUI, "Windows CE");
		SELECT(IMAGE_SUBSYSTEM_EFI_APPLICATION, "EFI Application");
		SELECT(IMAGE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER, "EFI driver with boot services");
		SELECT(IMAGE_SUBSYSTEM_EFI_RUNTIME_DRIVER, "EFI driver with run-time services");
		SELECT(IMAGE_SUBSYSTEM_EFI_ROM, "EFI ROM Image");
		SELECT(IMAGE_SUBSYSTEM_XBOX, "XBOX");
		SELECT(IMAGE_SUBSYSTEM_WINDOWS_BOOT_APPLICATION, "Boot Application");
	}

	char* DataDirectory[] = {
		"Export",
		"Import",
		"Resource",
		"Exception",
		"Certificate",
		"Base relocation",
		"Debug",
		"Architecture",
		"Global pointer reg",
		"Thread Local Storage",
		"Local config",
		"Bound import",
		"Import address",
		"Delay import desc",
		"The CLR header",
		"Reserved"
	};
	cout << "Data Directory : (Name) - (Virtual Address) (Size)" << endl;
	for(uint i = 0; i < pOptionalHeader->NumberOfRvaAndSizes; ++i) {
		printf("%s - 0x%-8X 0x%-8X\n", DataDirectory[i], pOptionalHeader->DataDirectory[i].VirtualAddress, pOptionalHeader->DataDirectory[i].Size);
	}

	cout << endl;

	// ===== IMAGE_SECTION_HEADER =====
	cout << ">>>>> IMAGE_SECTION_HEADER" << endl;

	// IMAGE_SECTION_HEADERのポインタ
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)((PBYTE)pOptionalHeader + sizeof(IMAGE_OPTIONAL_HEADER));
	for(int i = 0; i < pFileHeader->NumberOfSections; ++i) {
		cout << "### " << i << " [" << pSectionHeader[i].Name << "]" << endl;
		printf("Virtual Size : 0x%X\n", pSectionHeader[i].Misc.VirtualSize);
		printf("Virtual Address : 0x%X\n", pSectionHeader[i].VirtualAddress);
		printf("Raw Data Offset : 0x%X\n", pSectionHeader[i].PointerToRawData);
		printf("Raw Data Size : 0x%X\n", pSectionHeader[i].SizeOfRawData);
		printf("Relocation Offset : 0x%X\n", pSectionHeader[i].PointerToRelocations);
		printf("Relocations : 0x%X\n", pSectionHeader[i].NumberOfRelocations);
		printf("Line # Offset : 0x%X\n", pSectionHeader[i].PointerToLinenumbers);
		printf("Line #'s : 0x%X\n", pSectionHeader[i].NumberOfLinenumbers);
		cout << "Characteristics : " << endl;
		characteristics = pSectionHeader[i].Characteristics;
		COND(characteristics, IMAGE_SCN_TYPE_NO_PAD);
		COND(characteristics, IMAGE_SCN_CNT_CODE);
		COND(characteristics, IMAGE_SCN_CNT_INITIALIZED_DATA);
		COND(characteristics, IMAGE_SCN_CNT_UNINITIALIZED_DATA);
		COND(characteristics, IMAGE_SCN_LNK_OTHER);
		COND(characteristics, IMAGE_SCN_LNK_INFO);
		COND(characteristics, IMAGE_SCN_LNK_REMOVE);
		COND(characteristics, IMAGE_SCN_LNK_COMDAT);
		COND(characteristics, IMAGE_SCN_NO_DEFER_SPEC_EXC);
		COND(characteristics, IMAGE_SCN_GPREL);
		COND(characteristics, IMAGE_SCN_MEM_PURGEABLE);
		COND(characteristics, IMAGE_SCN_MEM_LOCKED);
		COND(characteristics, IMAGE_SCN_MEM_PRELOAD);
		COND(characteristics, IMAGE_SCN_ALIGN_1BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_2BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_4BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_8BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_16BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_32BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_64BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_128BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_256BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_512BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_1024BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_2048BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_4096BYTES);
		COND(characteristics, IMAGE_SCN_ALIGN_8192BYTES);
		COND(characteristics, IMAGE_SCN_LNK_NRELOC_OVFL);
		COND(characteristics, IMAGE_SCN_MEM_DISCARDABLE);
		COND(characteristics, IMAGE_SCN_MEM_NOT_CACHED);
		COND(characteristics, IMAGE_SCN_MEM_NOT_PAGED);
		COND(characteristics, IMAGE_SCN_MEM_SHARED);
		COND(characteristics, IMAGE_SCN_MEM_EXECUTE);
		COND(characteristics, IMAGE_SCN_MEM_READ);
		COND(characteristics, IMAGE_SCN_MEM_WRITE);
	}
	cout << endl;

	// Other
	PIMAGE_DATA_DIRECTORY pDataDirectory = &pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
	PIMAGE_SECTION_HEADER pISH = nullptr;

	for(int i = 0; i < pFileHeader->NumberOfSections; ++i) {
		if(pDataDirectory->VirtualAddress >= pSectionHeader[i].VirtualAddress &&
			pDataDirectory->VirtualAddress < pSectionHeader[i].VirtualAddress + pSectionHeader[i].VirtualAddress) {
			pISH = &pSectionHeader[i];
		}
	}
	if(pISH != nullptr) {
		DWORD dwDelta = pISH->VirtualAddress - pISH->PointerToRawData;
		if(pDataDirectory->VirtualAddress - dwDelta < pOptionalHeader->SizeOfImage) {
			PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)((PBYTE)lpFileBase + pDataDirectory->VirtualAddress - dwDelta);
			cout << "Import Table : " << endl;

			for(int i = 0; pImportDescriptor[i].OriginalFirstThunk || pImportDescriptor[i].FirstThunk; ++i) {
				if(pImportDescriptor[i].Name - dwDelta < pOptionalHeader->SizeOfImage) {
					cout << (LPCSTR)((PBYTE)lpFileBase + pImportDescriptor[i].Name - dwDelta) << endl;
				}
				printf("OriginalFirstThunk : 0x%X\n", pImportDescriptor[i].OriginalFirstThunk);
				printf("TimeDateStamp : 0x%X\n", pImportDescriptor[i].TimeDateStamp);
				printf("ForwarderChain : 0x%X\n", pImportDescriptor[i].ForwarderChain);
				printf("FirstThunk : 0x%X\n", pImportDescriptor[i].FirstThunk);

				if(pImportDescriptor[i].OriginalFirstThunk) {
					if(pImportDescriptor[i].OriginalFirstThunk - dwDelta >= pOptionalHeader->SizeOfImage ||
						pImportDescriptor[i].FirstThunk - dwDelta >= pOptionalHeader->SizeOfImage) goto ID_END;

					PIMAGE_THUNK_DATA32 pOFT = (PIMAGE_THUNK_DATA32)((PBYTE)lpFileBase + pImportDescriptor[i].OriginalFirstThunk - dwDelta);
					PIMAGE_THUNK_DATA32 pIAT = (PIMAGE_THUNK_DATA32)((PBYTE)lpFileBase + pImportDescriptor[i].FirstThunk - dwDelta);
					for(int j = 0; *((PDWORD)pOFT + j); ++j) {
						if(*((PDWORD)pOFT + j) & 0x80000000) {
							cout << (*((PDWORD)pOFT + j) & 0x0000FFFF) << endl;
						} else {
							if(*((PDWORD)pOFT + j) - dwDelta < pOptionalHeader->SizeOfImage) {
								PIMAGE_IMPORT_BY_NAME pImportByName = (PIMAGE_IMPORT_BY_NAME)((PBYTE)lpFileBase + *((PDWORD)pOFT + j) - dwDelta);
								cout << pImportByName->Hint << " " << pImportByName->Name << endl;
							}
						}

						printf("(IAT: 0x%X)\n", pIAT[j].u1.Function);
					}
				} else if(pImportDescriptor[i].FirstThunk) {
					if(pImportDescriptor[i].FirstThunk - dwDelta >= pOptionalHeader->SizeOfImage) goto ID_END;

					PIMAGE_THUNK_DATA32 pIAT = (PIMAGE_THUNK_DATA32)((PBYTE)lpFileBase + pImportDescriptor[i].FirstThunk - dwDelta);
					for(int j = 0; j < *((PDWORD)pIAT + j); ++j) {
						if(*((PDWORD)pIAT + j) & 0x80000000) {
							cout << (*((PDWORD)pIAT + j) & 0x0000FFFF) << endl;
						} else {
							if(*((PDWORD)pIAT + j) - dwDelta < pOptionalHeader->SizeOfImage) {
								PIMAGE_IMPORT_BY_NAME pImportByName = (PIMAGE_IMPORT_BY_NAME)((PBYTE)lpFileBase + *((PDWORD)pIAT + j) - dwDelta);
								cout << pImportByName->Hint << " " << pImportByName->Name << endl;
							}
						}

						printf("(IAT: 0x%X)\n", pIAT[j].u1.Function);
					}
				}
			}
		} else {
			cout << "No Imports Table Found" << endl;
		}
	} else {
		cout << "No Imports Table Found" << endl;
	}
	ID_END:

	cout << "[End]" << endl;

	CloseHandle(hFileMap);
	CloseHandle(hFile);
	return 0;
}
